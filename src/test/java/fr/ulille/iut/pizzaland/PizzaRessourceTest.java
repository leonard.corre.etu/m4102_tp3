package fr.ulille.iut.pizzaland;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.ulille.iut.pizzaland.beans.Ingredient;
import fr.ulille.iut.pizzaland.beans.Pizza;
import fr.ulille.iut.pizzaland.dao.IngredientDao;
import fr.ulille.iut.pizzaland.dao.PizzaDao;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.core.Application;
import jakarta.ws.rs.core.GenericType;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;

public class PizzaRessourceTest extends JerseyTest {
	private static final Logger LOGGER = Logger.getLogger(PizzaRessourceTest.class.getName());
	private PizzaDao dao;
	private IngredientDao daoIng;

	@Override
	protected Application configure() {
		BDDFactory.setJdbiForTests();

		return new ApiV1();
	}

	@Before
	public void setEnvUp() {
		dao = BDDFactory.buildDao(PizzaDao.class);
		dao.createTableAndPizzaAssociation();
		daoIng = BDDFactory.buildDao(IngredientDao.class);
		daoIng.createTable();
	}

	@After
	public void tearEnvDown() throws Exception {
		dao.dropTableAssociationPizza();
		daoIng.dropTable();
	}

	@Test
	public void testGetEmptyList() {
		Response response = target("/pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());
		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		assertEquals(0, pizzas.size());
	}

	@Test
	public void testGetPizzaList() {
		List<PizzaDto> pizzaList = new ArrayList<>();
		List<Ingredient> ingredients = new ArrayList<>();
		Ingredient ingre = new Ingredient("Mozza");
		ingredients.add(ingre);

		Pizza p1 = new Pizza("Reine");
		p1.setIngredients(ingredients);

		PizzaDto pdto1 = Pizza.toDto(p1);
		pizzaList.add(pdto1);

		Pizza p2 = new Pizza("Chorizo");
		p2.setIngredients(ingredients);

		PizzaDto pdto2 = Pizza.toDto(p2);
		pizzaList.add(pdto2);

		daoIng.insert(ingre);

		dao.insertPizzaAndAssociation(p1);
		dao.insertPizzaAndAssociation(p2);

		Response response = target("/pizzas").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		List<PizzaDto> pizzas;
		pizzas = response.readEntity(new GenericType<List<PizzaDto>>() {
		});

		System.out.println("\n\n" + Pizza.fromDto(pizzas.get(0)));
		assertEquals(p1.getName(), Pizza.fromDto(pizzas.get(0)).getName());
		assertEquals(p2.getName(), Pizza.fromDto(pizzas.get(1)).getName());
	}

	@Test
	public void testGetNotExistingIngredient() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().get();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetExistingPizza() {

		Pizza pizza = new Pizza("Reine");
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		Ingredient ingre = new Ingredient("Mozza");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		ingre = new Ingredient("Tomate");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		ingre = new Ingredient("Oignons");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		pizza.setIngredients(ingredients);

		dao.insertPizzaAndAssociation(pizza);


		Response response = target("/pizzas").path(pizza.getId().toString()).request(MediaType.APPLICATION_JSON).get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		Pizza result = Pizza.fromDto(response.readEntity(PizzaDto.class));
		System.out.println("\n\n" + result);
		assertEquals(pizza.getName(), result.getName());
	}

	@Test
	public void testCreatePizzaWithIngredient() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		List<Ingredient> ingredients = new ArrayList<Ingredient>();
		Ingredient ingre = new Ingredient("Mozza");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		ingre = new Ingredient("Tomate");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		ingre = new Ingredient("Oignons");
		ingredients.add(ingre);
		daoIng.insert(ingre);
		pizza.setIngredients(ingredients);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CREATED.getStatusCode(), response.getStatus());

		PizzaDto returnedEntity = response.readEntity(PizzaDto.class);

		assertEquals(target("/pizzas/" + returnedEntity.getId()).getUri(), response.getLocation());
		assertEquals(pizzaCreateDto.getName(), returnedEntity.getName());
	}

	@Test
	public void testCreateSamePizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		dao.insert(pizza);

		PizzaCreateDto pizzaCreateDto = Pizza.toCreateDto(pizza);
		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.CONFLICT.getStatusCode(), response.getStatus());
	}

	@Test
	public void testCreateIngredientWithoutName() {
		PizzaCreateDto pizzaCreateDto = new PizzaCreateDto();

		Response response = target("/pizzas").request().post(Entity.json(pizzaCreateDto));

		assertEquals(Response.Status.NOT_ACCEPTABLE.getStatusCode(), response.getStatus());
	}

	@Test
	public void testDeleteExistingPizza() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		dao.insert(pizza);

		Response response = target("/pizzas/").path(pizza.getId().toString()).request().delete();

		assertEquals(Response.Status.ACCEPTED.getStatusCode(), response.getStatus());

		Pizza result = dao.findById(pizza.getId());
		assertEquals(result, null);
	}

	@Test
	public void testDeleteNotExistingPizza() {
		Response response = target("/pizzas").path(UUID.randomUUID().toString()).request().delete();
		assertEquals(Response.Status.NOT_FOUND.getStatusCode(), response.getStatus());
	}

	@Test
	public void testGetPizzaName() {
		Pizza pizza = new Pizza();
		pizza.setName("Reine");
		dao.insert(pizza);

		Response response = target("pizzas").path(pizza.getId().toString()).path("name").request().get();

		assertEquals(Response.Status.OK.getStatusCode(), response.getStatus());

		assertEquals("Reine", response.readEntity(String.class));
	}
}