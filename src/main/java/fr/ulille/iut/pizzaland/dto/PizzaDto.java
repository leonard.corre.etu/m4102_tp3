package fr.ulille.iut.pizzaland.dto;

import java.util.Collection;
import java.util.List;
import java.util.UUID;

import fr.ulille.iut.pizzaland.beans.Ingredient;

public class PizzaDto {
  private UUID id;
  private List<Ingredient> ingredients;
  private String name;

  public List<Ingredient> getIngredients() {
	return ingredients;
}

public void setIngredients(List<Ingredient> ingredients) {
	this.ingredients = ingredients;
}

public PizzaDto() {}

  public UUID getId() {
    return id;
  }

  public void setId(UUID uuid) {
    this.id = uuid;
  }

  public boolean add(Ingredient arg0) {
	return ingredients.add(arg0);
}

public boolean addAll(Collection<? extends Ingredient> arg0) {
	return ingredients.addAll(arg0);
}

public void clear() {
	ingredients.clear();
}

public Ingredient get(int arg0) {
	return ingredients.get(arg0);
}

public void setName(String name) {
    this.name = name;
  }

  public String getName() {
	return name;
  }
}
