package fr.ulille.iut.pizzaland.dto;

import java.util.UUID;

public class IngredientDto {
  private UUID id;
  private String name;

  public IngredientDto() {}

  public UUID getId() {
    return id;
  }

  public void setId(UUID uuid) {
    this.id = uuid;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getName() {
	return name;
  }
}
