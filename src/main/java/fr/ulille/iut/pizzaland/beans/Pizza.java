package fr.ulille.iut.pizzaland.beans;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Spliterator;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Stream;

import fr.ulille.iut.pizzaland.dto.IngredientCreateDto;
import fr.ulille.iut.pizzaland.dto.IngredientDto;
import fr.ulille.iut.pizzaland.dto.PizzaCreateDto;
import fr.ulille.iut.pizzaland.dto.PizzaDto;

public class Pizza {
	private UUID id = UUID.randomUUID();
	private String name;
	private List<Ingredient> ingredients;

	public Pizza() {
		this.ingredients = new ArrayList<>();
	}

	public Pizza(String name) {
		this.name = name;
		this.ingredients = new ArrayList<>();
		
	}

	public Pizza(UUID id, String name) {
		this.id = id;
		this.name = name;
		this.ingredients = new ArrayList<>();
	}
	public Pizza(UUID id, String name, List<Ingredient> ingredients) {
		this.id = id;
		this.name = name;
		this.ingredients = ingredients;
	}

	public boolean add(Ingredient arg0) {
		return ingredients.add(arg0);
	}

	public void add(int arg0, Ingredient arg1) {
		ingredients.add(arg0, arg1);
	}

	public boolean addAll(Collection<? extends Ingredient> arg0) {
		return ingredients.addAll(arg0);
	}

	public boolean addAll(int arg0, Collection<? extends Ingredient> arg1) {
		return ingredients.addAll(arg0, arg1);
	}

	public void clear() {
		ingredients.clear();
	}

	public boolean contains(Object arg0) {
		return ingredients.contains(arg0);
	}

	public boolean containsAll(Collection<?> arg0) {
		return ingredients.containsAll(arg0);
	}

	public void forEach(Consumer<? super Ingredient> arg0) {
		ingredients.forEach(arg0);
	}

	public Ingredient get(int arg0) {
		return ingredients.get(arg0);
	}

	public int indexOf(Object arg0) {
		return ingredients.indexOf(arg0);
	}

	public boolean isEmpty() {
		return ingredients.isEmpty();
	}

	public Iterator<Ingredient> iterator() {
		return ingredients.iterator();
	}

	public int lastIndexOf(Object arg0) {
		return ingredients.lastIndexOf(arg0);
	}

	public ListIterator<Ingredient> listIterator() {
		return ingredients.listIterator();
	}

	public ListIterator<Ingredient> listIterator(int arg0) {
		return ingredients.listIterator(arg0);
	}

	public Stream<Ingredient> parallelStream() {
		return ingredients.parallelStream();
	}

	public Ingredient remove(int arg0) {
		return ingredients.remove(arg0);
	}

	public boolean remove(Object arg0) {
		return ingredients.remove(arg0);
	}

	public boolean removeAll(Collection<?> arg0) {
		return ingredients.removeAll(arg0);
	}

	public boolean removeIf(Predicate<? super Ingredient> filter) {
		return ingredients.removeIf(filter);
	}

	public void replaceAll(UnaryOperator<Ingredient> operator) {
		ingredients.replaceAll(operator);
	}

	public boolean retainAll(Collection<?> arg0) {
		return ingredients.retainAll(arg0);
	}

	public Ingredient set(int arg0, Ingredient arg1) {
		return ingredients.set(arg0, arg1);
	}

	public int size() {
		return ingredients.size();
	}

	public void sort(Comparator<? super Ingredient> arg0) {
		ingredients.sort(arg0);
	}

	public Spliterator<Ingredient> spliterator() {
		return ingredients.spliterator();
	}

	public Stream<Ingredient> stream() {
		return ingredients.stream();
	}

	public List<Ingredient> subList(int arg0, int arg1) {
		return ingredients.subList(arg0, arg1);
	}

	public Object[] toArray() {
		return ingredients.toArray();
	}

	public <T> T[] toArray(IntFunction<T[]> generator) {
		return ingredients.toArray(generator);
	}

	public <T> T[] toArray(T[] arg0) {
		return ingredients.toArray(arg0);
	}

	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public void setIngredients(List<Ingredient> ingredients) {
		this.ingredients = ingredients;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static PizzaDto toDto(Pizza i) {
		PizzaDto dto = new PizzaDto();
		dto.setId(i.getId());
		dto.setName(i.getName());

		return dto;
	}

	public static Pizza fromDto(PizzaDto pizzaDto) {
		Pizza ingredient = new Pizza();
		ingredient.setId(pizzaDto.getId());
		ingredient.setName(pizzaDto.getName());

		return ingredient;
	}

	@Override
	public String toString() {
		return "Ingredient [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pizza other = (Pizza) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

	public static PizzaCreateDto toCreateDto(Pizza pizza) {
		PizzaCreateDto dto = new PizzaCreateDto();
		dto.setName(pizza.getName());

		return dto;
	}

	public static Pizza fromPizzaCreateDto(PizzaCreateDto dto) {
		Pizza pizza = new Pizza();
		pizza.setName(dto.getName());

		return pizza;
	}

}
